/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoes.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author guilherme.borges
 */
@Entity
@Table(name = "BANDEIRA")
public class Bandeira {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "bandeira_seq", sequenceName = "bandeira_seq")
    @GeneratedValue(generator = "bandeira_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;
    
    private Double taxa;
    
    @OneToMany(mappedBy = "bandeira")
    private List<Cartao> cartoes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public List<Cartao> getCartoes() {
        return cartoes;
    }

    public void pushCartoes(Cartao... cartoes) {
        this.cartoes.addAll(Arrays.asList(cartoes));
    }
    
    
    
}
