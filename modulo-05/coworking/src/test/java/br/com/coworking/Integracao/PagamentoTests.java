package br.com.coworking.Integracao;

import br.com.coworking.Controllers.*;
import br.com.coworking.CoworkingApplicationTests;
import br.com.coworking.Entity.*;
import br.com.coworking.Enum.TipoContratacao;
import br.com.coworking.Enum.TipoPagamento;
import br.com.coworking.Repository.SaldoClienteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
public class PagamentoTests extends CoworkingApplicationTests {

	private MockMvc mvc;

	TipoContato email = new TipoContato();
	TipoContato telefone = new TipoContato();
	Contato contato1 = new Contato();
	Contato contato2 = new Contato();
	Cliente cliente1 = new Cliente();
	Espaco espaco1 = new Espaco();
	Contratacao contratacaoA = new Contratacao();
	Pagamento pagamento = new Pagamento();

	@Autowired
	TipoContatoController tipoContatoController;

	@Autowired
	ContatoController contatoController;

	@Autowired
	ClienteController clienteController;

	@Autowired
	EspacoController espacoController;

	@Autowired
	ContratacaoController contratacaoController;

	@Autowired
	PagamentoController pagamentoController;

	@Autowired
	SaldoClienteRepository saldoClienteRepository;

	@Autowired
	ApplicationContext context;

	@Autowired
	ObjectMapper mapper;

	@Before
	public void setUp() throws Exception{
		this.mvc = MockMvcBuilders.standaloneSetup(tipoContatoController, contatoController, clienteController, espacoController, contratacaoController, pagamentoController).build();

		//salvando tipoContato
		email.setNome("email");
		telefone.setNome("telefone");
		this.mvc.perform(post("/api/tipocontato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(email)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/tipocontato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(telefone)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		//salvando contatos
		contato1.setValor("dbc@gmail.com");
		contato1.setTipoContato(new TipoContato());
		contato1.getTipoContato().setId(1);
		contato2.setValor("959565659");
		contato2.setTipoContato(new TipoContato());
		contato2.getTipoContato().setId(2);
		this.mvc.perform(post("/api/contato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(contato1)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/contato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(contato2)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		Contato contatoEmail = new Contato();
		contatoEmail.setId(1);
		Contato contatoTelefone = new Contato();
		contatoTelefone.setId(2);

		//criando cliente1
		cliente1.setNome("DBC");
		cliente1.setCpf("59521908477");
		cliente1.setDataNascimento(new Date(System.currentTimeMillis()));
		cliente1.pushContatos(contatoEmail, contatoTelefone);

		//criando espaco1
		espaco1.setNome("Mind Room");
		espaco1.setQtdPessoas(20);
		espaco1.setValor(100.0);

	}

	@Test
	public void eBuscarTodos() throws Exception{
		this.mvc.perform(get("/api/pagamento/"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void salvarPagamentoGeraSaldoCliente() throws Exception {
		this.mvc.perform(post("/api/espaco/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(espaco1))).andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/cliente/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(cliente1)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		Espaco espacoNoBanco =  new Espaco();
		espacoNoBanco.setId(1);
		Cliente clienteNoBanco = new Cliente();
		clienteNoBanco.setId(1);

		contratacaoA.setEspaco(espacoNoBanco);
		contratacaoA.setCliente(clienteNoBanco);
		contratacaoA.setTipoContratacao(TipoContratacao.DIARIA);
		contratacaoA.setQuantidade(2);
		contratacaoA.setPrazo(3);

		this.mvc.perform(post("/api/contratacao/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(contratacaoA))).andExpect(MockMvcResultMatchers.status().isOk());

		Contratacao contratacaoNoBanco = new Contratacao();
		contratacaoNoBanco.setId(1);
		pagamento.setContratacao(contratacaoNoBanco);
		pagamento.setTipoPagamento(TipoPagamento.DINHEIRO);

		MvcResult result = this.mvc.perform(post("/api/pagamento/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(pagamento))).andReturn();

		SaldoClienteId id = new SaldoClienteId(1, 1);
		SaldoCliente saldo = saldoClienteRepository.findById(id).get();

		assertEquals("Pagamento registrado com sucesso", result.getResponse().getContentAsString());
		assertEquals(TipoContratacao.DIARIA, saldo.getTipoContratacao());
		assertEquals(2, (int) saldo.getQuantidade());
	}

	@Test
	public void salvarPagamentoSemContratacaoESemPacote() throws Exception{
		Pagamento pagamentoInvalido = new Pagamento();
		pagamentoInvalido.setTipoPagamento(TipoPagamento.DINHEIRO);

		MvcResult result = this.mvc.perform(post("/api/pagamento/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(pagamentoInvalido))).andReturn();

		assertEquals("Sempre ou id_cliente_pacote ou id_contratacao deve ser nulo", result.getResponse().getContentAsString());
	}

	@Test
	public void salvarPagamentoComContratacaoEPacote() throws Exception{
		Pagamento pagamentoInvalido2 = new Pagamento();
		pagamentoInvalido2.setTipoPagamento(TipoPagamento.DINHEIRO);
		pagamentoInvalido2.setContratacao(new Contratacao());
		pagamentoInvalido2.setClientePacote(new ClientePacote());

		MvcResult result = this.mvc.perform(post("/api/pagamento/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(pagamentoInvalido2))).andReturn();

		assertEquals("Sempre ou id_cliente_pacote ou id_contratacao deve ser nulo", result.getResponse().getContentAsString());
	}



}
