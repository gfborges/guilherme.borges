package br.com.coworking.Integracao;

import br.com.coworking.Controllers.ClienteController;
import br.com.coworking.Controllers.ContatoController;
import br.com.coworking.Controllers.EspacoController;
import br.com.coworking.Controllers.TipoContatoController;
import br.com.coworking.CoworkingApplicationTests;
import br.com.coworking.Entity.Cliente;
import br.com.coworking.Entity.Contato;
import br.com.coworking.Entity.Espaco;
import br.com.coworking.Entity.TipoContato;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
public class EspacoTests extends CoworkingApplicationTests {

	private MockMvc mvc;

	Espaco espaco1 = new Espaco();
	Espaco espaco2 = new Espaco();
	Espaco espaco3 = new Espaco();

	@Autowired
	EspacoController espacoController;

	@Autowired
	ApplicationContext context;

	@Autowired
	ObjectMapper mapper;

	@Before
	public void setUp() {
		espaco1.setNome("Mind Room");
		espaco1.setQtdPessoas(20);
		espaco1.setValor(100.0);

		espaco2.setNome("Mind Room");
		espaco2.setQtdPessoas(30);
		espaco2.setValor(150.0);

		espaco3.setNome("Cozinha");
		espaco3.setQtdPessoas(15);
		espaco3.setValor(120.0);

		this.mvc = MockMvcBuilders.standaloneSetup(espacoController).build();
	}

	@Test
	public void eBuscarTodos() throws Exception{
		this.mvc.perform(get("/api/espaco/"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void salvarEspacoANovo() throws Exception {
		this.mvc.perform(post("/api/espaco/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(espaco1))).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void salvarEspacoBComMesmoNome() throws Exception {
		String resposta = "start";
		try {
			MvcResult result = this.mvc.perform(post("/api/espaco/salvar").contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsString(espaco2))).andReturn();
		} catch (NestedServletException e) {
			resposta = "Falha ao salvar espaco";
		}

		assertEquals("Falha ao salvar espaco", resposta);
	}

	@Test
	public void salvarEspacoComNomeDiferente() throws Exception {
		this.mvc.perform(post("/api/espaco/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(espaco3))).andExpect(MockMvcResultMatchers.status().isOk());
	}

}
