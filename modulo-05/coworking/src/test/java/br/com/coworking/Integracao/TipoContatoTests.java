package br.com.coworking.Integracao;

import br.com.coworking.Controllers.TipoContatoController;
import br.com.coworking.CoworkingApplicationTests;
import br.com.coworking.Entity.TipoContato;
import br.com.coworking.Repository.TipoContatoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
public class TipoContatoTests extends CoworkingApplicationTests {

	private MockMvc mvc;

	@Autowired
	TipoContatoController tipoContatoController;

	@MockBean
	private TipoContatoRepository tipoContatoRepository;

	@Autowired
	ApplicationContext context;

	@Before
	public void setUp() {
		TipoContato email = new TipoContato();
		email.setNome("email");
		TipoContato facebook = new TipoContato();
		facebook.setNome("facebook");
		TipoContato telefone = new TipoContato();
		telefone.setNome("telefone");

		Mockito.when(tipoContatoRepository.findByNome(email.getNome())).thenReturn(email);
		this.mvc = MockMvcBuilders.standaloneSetup(tipoContatoController).build();
	}

	@Test
	public void eBuscarTodos() throws Exception{
		this.mvc.perform(get("/api/tipocontato/"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void buscaPorNome() {
		TipoContato found = tipoContatoRepository.findByNome("email");
		assertEquals("email", found.getNome());
	}
}
