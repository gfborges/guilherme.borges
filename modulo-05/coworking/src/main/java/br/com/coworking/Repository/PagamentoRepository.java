package br.com.coworking.Repository;

import br.com.coworking.Entity.ClientePacote;
import br.com.coworking.Entity.Contratacao;
import br.com.coworking.Entity.Pagamento;
import br.com.coworking.Enum.TipoPagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer>{
	List<Pagamento> findAllByClientePacote(ClientePacote clientePacote);
	List<Pagamento> findAllByContratacao(Contratacao contratacao);
	List<Pagamento> findAllByTipoPagamento(TipoPagamento tipoPagamento);
}
