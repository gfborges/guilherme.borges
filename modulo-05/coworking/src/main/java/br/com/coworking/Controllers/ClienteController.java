package br.com.coworking.Controllers;

import br.com.coworking.Entity.Cliente;
import br.com.coworking.Exceptions.ContatoInvalidoException;
import br.com.coworking.Services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {
	
	@Autowired
    ClienteService clienteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Cliente> listarTodos() {
		return clienteService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Cliente buscarPorID(@PathVariable Integer id) {
		return clienteService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public String salvarNovo(@RequestBody Cliente cliente) {
		try {
			clienteService.salvar(cliente);
		} catch (ContatoInvalidoException e) {
			return e.getMENSAGEM();
		}
		return "Cliente salvo com sucesso";
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Cliente editar(@PathVariable Integer id, @RequestBody Cliente cliente) {
		return clienteService.editar(id, cliente);
	}
}
