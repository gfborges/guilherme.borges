package br.com.coworking.Controllers;

import br.com.coworking.Entity.Usuario;
import br.com.coworking.Exceptions.SenhaInvalidaException;
import br.com.coworking.Services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {
	
	@Autowired
    UsuarioService usuarioService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Usuario> listarTodos() {
		return usuarioService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Usuario buscarPorID(@PathVariable Integer id) {
		return usuarioService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public String salvarNovo(@RequestBody Usuario usuario) {
		try {
			if (usuario.getSenha().length() < 6) throw new SenhaInvalidaException();
			usuarioService.salvar(usuario);
			return "Usuario cadastrado com sucesso";
		} catch (SenhaInvalidaException e) {
			return e.getMENSAGEM();
		} catch (Exception e) {
			return "Erro ao cadastrar usuario";
		}
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Usuario editar(@PathVariable Integer id, @RequestBody Usuario usuario) {
		return usuarioService.editar(id, usuario);
	}
}
