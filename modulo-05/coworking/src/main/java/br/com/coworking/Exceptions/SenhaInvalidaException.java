package br.com.coworking.Exceptions;

public class SenhaInvalidaException extends Exception {
    private final String MENSAGEM = "Senha deve ter tamanho minimo de 6 digitos";

    public String getMENSAGEM() {
        return MENSAGEM;
    }
}
