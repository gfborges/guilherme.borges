package br.com.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.coworking.Entity.Espaco;

public interface EspacoRepository extends CrudRepository<Espaco, Integer>{
	Espaco findByNome(String nome);
	List<Espaco> findAllByQtdPessoas(Integer qtdPessoas);
	List<Espaco> findAllByValor(Double valor);
}
