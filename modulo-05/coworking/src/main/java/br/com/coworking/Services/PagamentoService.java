package br.com.coworking.Services;

import br.com.coworking.Entity.*;
import br.com.coworking.Repository.ClientePacoteRepository;
import br.com.coworking.Repository.ContratacaoRepository;
import br.com.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PagamentoService {

	@Autowired
	public PagamentoRepository pagamentoRepository;

	@Autowired
	public ContratacaoRepository contratacaoRepository;

	@Autowired
	public ClientePacoteRepository clientePacoteRepository;

	@Autowired
	UtilService utilService;
	
	@Transactional(rollbackFor = Exception.class)
	public Pagamento salvar(Pagamento pagamento) {
		int idCliente;

		if (pagamento.getClientePacote() != null) {
			ClientePacote clientePacote = clientePacoteRepository.findById(pagamento.getClientePacote().getId()).get();
			idCliente = clientePacote.getCliente().getId();
			List<EspacoPacote> espacoPacotes = clientePacote.getPacote().getEspacoPacotes();
			int indice = 0;

			while (espacoPacotes.get(indice) != null) {
				utilService.gerarSaldoCliente(idCliente, espacoPacotes.get(indice), clientePacote.getQuantidade());
				indice++;
			}
		}

		if (pagamento.getContratacao() != null) {
			Contratacao contratacao = contratacaoRepository.findById(pagamento.getContratacao().getId()).get();
			idCliente = contratacao.getCliente().getId();
			utilService.gerarSaldoCliente(idCliente, contratacao, 1);
		}

		return pagamentoRepository.save(pagamento);
	}
	
	public Pagamento buscarPorID(Integer id) {
		if( pagamentoRepository.findById(id).isPresent() )
			return pagamentoRepository.findById(id).get();
		return null;
	}
	
	public List<Pagamento> buscarTodos() {
		return (List<Pagamento>) pagamentoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pagamento editar(Integer id, Pagamento pagamento) {
		pagamento.setId(id);
		return pagamentoRepository.save(pagamento);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Pagamento pagamento) {
		pagamentoRepository.delete(pagamento);
	}
	
}
