package br.com.coworking.Exceptions;

public class PagamentoInvalidoException extends Exception {
    private final String MENSAGEM = "Sempre ou id_cliente_pacote ou id_contratacao deve ser nulo";

    public String getMENSAGEM() {
        return MENSAGEM;
    }
}
