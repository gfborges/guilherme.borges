package br.com.coworking.Entity;

import br.com.coworking.Enum.TipoContratacao;
import br.com.coworking.Interfaces.Orcamento;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table(name = "ESPACO_PACOTE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EspacoPacote.class)
public class EspacoPacote implements Orcamento {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO")
    private Espaco espaco;

    @ManyToOne
    @JoinColumn(name = "ID_PACOTE")
    private Pacote pacote;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "TIPO_CONTRATACAO")
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
