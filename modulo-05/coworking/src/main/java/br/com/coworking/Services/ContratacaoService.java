package br.com.coworking.Services;

import br.com.coworking.Entity.Contratacao;
import br.com.coworking.Entity.Espaco;
import br.com.coworking.Repository.ContratacaoRepository;
import br.com.coworking.Repository.EspacoRepository;
import br.com.coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContratacaoService {

	@Autowired
	public ContratacaoRepository contratacaoRepository;

	@Autowired
	public EspacoRepository espacoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Double salvar(Contratacao contratacao) {
		contratacaoRepository.save(contratacao);
		Double valor = espacoRepository.findById(contratacao.getEspaco().getId()).get().getValor();
		return valor;
	}
	
	public Contratacao buscarPorID(Integer id) {
		if( contratacaoRepository.findById(id).isPresent() )
			return contratacaoRepository.findById(id).get();
		return null;
	}
	
	public List<Contratacao> buscarTodos() {
		return (List<Contratacao>) contratacaoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Contratacao editar(Integer id, Contratacao contratacao) {
		contratacao.setId(id);
		return contratacaoRepository.save(contratacao);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Contratacao contratacao) {
		contratacaoRepository.delete(contratacao);
	}
	
}
