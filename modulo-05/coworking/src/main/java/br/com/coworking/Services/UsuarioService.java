package br.com.coworking.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.coworking.Entity.Usuario;
import br.com.coworking.Repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	public UsuarioRepository usuarioRepository;

	@Autowired
	UtilService utilService;
	
	@Transactional(rollbackFor = Exception.class)
	public Usuario salvar(Usuario usuario) throws Exception{
		String senhaCriptografada = utilService.criptografarSenha(usuario.getSenha());
		usuario.setSenha(senhaCriptografada);
		return usuarioRepository.save(usuario);
	}
	
	public Usuario buscarPorID(Integer id) {
		if( usuarioRepository.findById(id).isPresent() )
			return usuarioRepository.findById(id).get();
		return null;
	}
	
	public List<Usuario> buscarTodos() {
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Usuario editar(Integer id, Usuario usuario) {
		usuario.setId(id);
		return usuarioRepository.save(usuario);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Usuario usuario) {
		usuarioRepository.delete(usuario);
	}
	
}
