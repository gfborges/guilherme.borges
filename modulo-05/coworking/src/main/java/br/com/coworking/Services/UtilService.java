package br.com.coworking.Services;

import br.com.coworking.Entity.Contratacao;
import br.com.coworking.Entity.SaldoCliente;
import br.com.coworking.Entity.SaldoClienteId;
import br.com.coworking.Enum.TipoContratacao;
import br.com.coworking.Interfaces.Orcamento;
import br.com.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Locale;

@Service
public class UtilService {

    @Autowired
    public SaldoClienteRepository saldoClienteRepository;

    private boolean isSaldoVencido(SaldoCliente saldoCliente) {
        Date dataAtual = new Date(System.currentTimeMillis());
        Date vencimento = saldoCliente.getVencimento();
        if ( dataAtual.after(vencimento) )
            return true;
        return false;
    }

    public SaldoCliente validarSaldoCliente(SaldoClienteId id) {
        SaldoCliente saldoCliente = saldoClienteRepository.findById(id).get();

        if( saldoCliente.getQuantidade() > 0 ) {
            if ( !isSaldoVencido(saldoCliente) )
                return saldoCliente;

            //o saldo vencido eu seto em zero e atualizo no banco
            else {
                saldoCliente.setQuantidade(0);
                saldoClienteRepository.save(saldoCliente);
            }
        }

        //null representa saldo invalido
        return null;
    }

    private SaldoCliente definirVencimento(SaldoCliente saldoCliente, int prazo, int qtdEspaco) {
        Date dataReferencia;
        if( saldoCliente.getVencimento() == null )
            dataReferencia = new Date(System.currentTimeMillis());
        else
            dataReferencia = saldoCliente.getVencimento();
        LocalDate vencimentoReferencia = dataReferencia.toLocalDate();
        vencimentoReferencia.plusDays(prazo * qtdEspaco);
        Date vencimentoNovo = new Date(vencimentoReferencia.toEpochDay());
        saldoCliente.setVencimento(vencimentoNovo);

        return saldoCliente;
    }

    private SaldoCliente definirSaldoCliente(SaldoCliente saldoCliente, Orcamento orcamento, int qtdContratada) {
        saldoCliente.setTipoContratacao(orcamento.getTipoContratacao());
        saldoCliente.setQuantidade(orcamento.getQuantidade() * qtdContratada);
        return definirVencimento(saldoCliente, orcamento.getPrazo(), qtdContratada);
    }

    public void gerarSaldoCliente(int idCliente, Orcamento orcamento, int qtdContratada) {
        SaldoCliente saldoCliente;
        SaldoClienteId id = new SaldoClienteId(idCliente, orcamento.getEspaco().getId());

        if (saldoClienteRepository.existsById(id)) {
            saldoCliente = saldoClienteRepository.findById(id).get();
        } else {
            saldoCliente = new SaldoCliente();
            saldoCliente.setId(id);
        }

        saldoCliente = definirSaldoCliente(saldoCliente, orcamento, qtdContratada);
        saldoClienteRepository.save(saldoCliente);
    }

    public Double calcularCustoContratacao(Double valorEspaco, Contratacao contratacao) {
        Double desconto = contratacao.getDesconto() == null ? 0 : contratacao.getDesconto();
        TipoContratacao tipoContratacao = contratacao.getTipoContratacao();
        int quantidade = contratacao.getQuantidade();
        Double custoHora = valorEspaco * quantidade;
        Double custoContratacao;

        switch (tipoContratacao) {
            case MINUTO:
                custoContratacao = custoHora / 60;
                break;
            case TURNO:
                custoContratacao = custoHora * 5;
                break;
            case DIARIA:
                custoContratacao = custoHora * 8;
                break;
            case SEMANA:
                custoContratacao = custoHora * 44;
                break;
            case MES:
                custoContratacao = custoHora * 220;
                break;
            default:
                custoContratacao = custoHora;
        }
        return custoContratacao - desconto;
    }

    public String valorToMoeda(Double valor) {
        return NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(valor);
    }

    public Double moedaToValor(String moeda) throws ParseException {
        NumberFormat form = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
        return (Double) form.parse(moeda.replaceAll("[^\\d.,]", ""));
    }

    public String criptografarSenha(String senha) throws Exception{
        MessageDigest codificador = MessageDigest.getInstance("MD5");

        codificador.update(senha.getBytes("UTF-8"));

        BigInteger senhaCriptografada = new BigInteger(1, codificador.digest());

        return String.format("%1$032X", senhaCriptografada);
    }
}
