package br.com.coworking.Enum;

public enum TipoContratacao {

    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES;

}
