package br.com.coworking.Exceptions;

public class ContatoInvalidoException extends Exception {
    private final String MENSAGEM = "Cliente deve informar um contato do tipo email e um do tipo telefone";

    public String getMENSAGEM() {
        return MENSAGEM;
    }
}
