package br.com.coworking.Repository;

import br.com.coworking.Entity.Cliente;
import br.com.coworking.Entity.ClientePacote;
import br.com.coworking.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Integer> {
	List<ClientePacote> findAllByCliente(Cliente cliente);
	List<ClientePacote> findAllByPacote(Pacote pacote);
}
