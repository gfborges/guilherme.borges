package br.com.coworking.Controllers;

import br.com.coworking.Entity.Contratacao;
import br.com.coworking.Services.ContratacaoService;
import br.com.coworking.Services.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {
	
	@Autowired
    ContratacaoService contratacaoService;

	@Autowired
	UtilService utilService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Contratacao> listarTodos() {
		return contratacaoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Contratacao buscarPorID(@PathVariable Integer id) {
		return contratacaoService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Double salvarNovo(@RequestBody Contratacao contratacao) {
		Double valorEspaco = contratacaoService.salvar(contratacao);
		return utilService.calcularCustoContratacao(valorEspaco, contratacao);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Contratacao editar(@PathVariable Integer id, @RequestBody Contratacao contratacao) {
		return contratacaoService.editar(id, contratacao);
	}
}
