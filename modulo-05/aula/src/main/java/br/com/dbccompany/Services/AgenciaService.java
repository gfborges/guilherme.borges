package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Repository.AgenciaRepository;

@Service
public class AgenciaService {
	
	@Autowired
	private AgenciaRepository agenciaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia salvar(Agencia agencia) {
		return agenciaRepository.save(agencia);
	}
	
	private Agencia buscarAgencia(Integer id) {
		if ( agenciaRepository.findById(id).isPresent() )
			return agenciaRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private Agencia editarAgencia(Integer id, Agencia agencia) {
		agencia.setId(id);
		return agenciaRepository.save(agencia);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarAgencia(Agencia agencia) {
		agenciaRepository.delete(agencia);
	}
}
