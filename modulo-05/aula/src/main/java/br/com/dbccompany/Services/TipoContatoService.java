package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;

@Service
public class TipoContatoService {

	@Autowired
	private TipoContatoRepository tipoContatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private TipoContato salvar(TipoContato tipoContato) {
		return tipoContatoRepository.save(tipoContato);
	}
	
	private TipoContato buscarTipoContato(Integer id) {
		if ( tipoContatoRepository.findById(id).isPresent() )
			return tipoContatoRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private TipoContato editarTipoContato(Integer id, TipoContato tipoContato) {
		tipoContato.setId(id);
		return tipoContatoRepository.save(tipoContato);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarTipoContato(TipoContato tipoContato) {
		tipoContatoRepository.delete(tipoContato);
	}
}
