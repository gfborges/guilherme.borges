package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.EnderecoRepository;

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Endereco salvar(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}
	
	private Endereco buscarEndereco(Integer id) {
		if ( enderecoRepository.findById(id).isPresent() )
			return enderecoRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private Endereco editarEndereco(Integer id, Endereco endereco) {
		endereco.setId(id);
		return enderecoRepository.save(endereco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarEndereco(Endereco endereco) {
		enderecoRepository.delete(endereco);
	}
}
