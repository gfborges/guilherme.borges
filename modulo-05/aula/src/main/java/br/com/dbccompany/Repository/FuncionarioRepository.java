package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Funcionario;

public interface FuncionarioRepository extends CrudRepository<Funcionario, Integer>{
	List<Funcionario> findByCargo(String cargo);	
}
