package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "EMPRESTIMO")
public class Emprestimo {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "ID_CONTA")
	private Conta conta;
	
	private Integer valor;
	
	@OneToMany(mappedBy = "emprestimo")
    private List<EmprestimoAprovado> emprestimosAprovados = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	public List<EmprestimoAprovado> getEmprestimosAprovados() {
        return emprestimosAprovados;
    }

    public void pushEmprestimosAprovados(EmprestimoAprovado... emprestimosAprovados) {
        this.emprestimosAprovados.addAll(Arrays.asList(emprestimosAprovados));
    }
}
