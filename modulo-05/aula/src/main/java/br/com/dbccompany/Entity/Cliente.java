package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE")
@PrimaryKeyJoinColumn(name = "ID_USUARIO")
public class Cliente extends Usuario{
    
    @ManyToMany(mappedBy = "clientes")
    private List<Conta> contas = new ArrayList<>();    

    public List<Conta> getContas() {
        return contas;
    }

    public void pushContas(Conta... contas) {
        this.contas.addAll(Arrays.asList(contas));
    }
    
    
}

