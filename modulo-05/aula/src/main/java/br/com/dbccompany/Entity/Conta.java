package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
@Table(name = "CONTA")
public class Conta {
    
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "ID_AGENCIA")
    private Agencia agencia;
    
    private Integer numero;
    
    private Integer saldo;
    
    @OneToOne
    @JoinColumn(name = "ID_GERENTE")
    private Funcionario gerente;
    
    @Column(name = "CREDITO_PRE_APROVADO")
    private Integer creditoPreAprovado;
    
    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "TIPO_CONTA", nullable = false)
    private TipoConta tipoConta;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CONTA_CLIENTE", joinColumns = {
        @JoinColumn(name = "ID_CONTA")}, inverseJoinColumns = {
            @JoinColumn(name = "ID_CLIENTE")})
    private List<Cliente> clientes = new ArrayList<>();
    
    @OneToMany(mappedBy = "contaOrigem")
    private List<Movimentacao> lancamentosEfetuados = new ArrayList<>();
    
    @OneToMany(mappedBy = "contaDestino")
    private List<Movimentacao> lancamentosRecebidos = new ArrayList<>();
    
    @OneToMany(mappedBy = "conta")
    private List<Emprestimo> emprestimos = new ArrayList<>();
    
    public Integer getCreditoPreAprovado() {
        return creditoPreAprovado;
    }

    public void setCreditoPreAprovado(Integer creditoPreAprovado) {
        this.creditoPreAprovado = creditoPreAprovado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getSaldo() {
        return saldo;
    }

    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }    

    public Funcionario getGerente() {
		return gerente;
	}

	public void setGerente(Funcionario gerente) {
		this.gerente = gerente;
	}

	public List<Cliente> getClientes() {
        return clientes;
    }

    public void pushClientes(Cliente... clientes) {
        this.clientes.addAll(Arrays.asList(clientes));
    }

    public List<Movimentacao> getLancamentosEfetuados() {
        return lancamentosEfetuados;
    }

    public void pushLancamentosEfetuados(Movimentacao... lancamentosEfetuados) {
        this.lancamentosEfetuados.addAll(Arrays.asList(lancamentosEfetuados));
    }

    public List<Movimentacao> getLancamentosRecebidos() {
        return lancamentosRecebidos;
    }

    public void pushLancamentosRecebidos(Movimentacao... lancamentosRecebidos) {
        this.lancamentosRecebidos.addAll(Arrays.asList(lancamentosRecebidos));
    }
    
    public List<Emprestimo> getEmprestimos() {
        return emprestimos;
    }

    public void pushEmprestimos(Emprestimo... emprestimos) {
        this.emprestimos.addAll(Arrays.asList(emprestimos));
    }
    
}

