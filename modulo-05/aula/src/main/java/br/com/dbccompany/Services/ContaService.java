package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Repository.ContaRepository;

@Service
public class ContaService {

	@Autowired
	private ContaRepository contaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Conta salvar(Conta conta) {
		return contaRepository.save(conta);
	}
	
	private Conta buscarConta(Integer id) {
		if ( contaRepository.findById(id).isPresent() )
			return contaRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private Conta editarConta(Integer id, Conta conta) {
		conta.setId(id);
		return contaRepository.save(conta);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarConta(Conta conta) {
		contaRepository.delete(conta);
	}
}
