package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Cliente salvar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	private Cliente buscarCliente(Integer id) {
		if ( clienteRepository.findById(id).isPresent() )
			return clienteRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private Cliente editarCliente(Integer id, Cliente cliente) {
		cliente.setId(id);
		return clienteRepository.save(cliente);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarCliente(Cliente cliente) {
		clienteRepository.delete(cliente);
	}
}
