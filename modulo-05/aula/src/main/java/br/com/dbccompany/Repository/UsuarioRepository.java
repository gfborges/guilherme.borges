package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	Usuario findByNome(String nome);
	Usuario findByCpf(Long cpf);
	Usuario findByRg(Long rg);
	Usuario findByContatos(Contato contato);
	List<Usuario> findByEnderecos(Endereco endereco);	
}
