package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Funcionario;
import br.com.dbccompany.Repository.FuncionarioRepository;

@Service
public class FuncionarioService {

	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Funcionario salvar(Funcionario funcionario) {
		return funcionarioRepository.save(funcionario);
	}
	
	private Funcionario buscarFuncionario(Integer id) {
		if ( funcionarioRepository.findById(id).isPresent() )
			return funcionarioRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private Funcionario editarFuncionario(Integer id, Funcionario funcionario) {
		funcionario.setId(id);
		return funcionarioRepository.save(funcionario);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarFuncionario(Funcionario funcionario) {
		funcionarioRepository.delete(funcionario);
	}
}
