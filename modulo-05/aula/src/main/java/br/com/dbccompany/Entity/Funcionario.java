package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "FUNCIONARIO")
@PrimaryKeyJoinColumn(name = "ID_USUARIO")
public class Funcionario extends Usuario{

	private String cargo;
	
	@ManyToOne
    @JoinColumn(name = "ID_BANCO")
    private Banco banco;
	
	@OneToOne(mappedBy = "gerenteGeral")
	private Agencia agencia;
	
	@OneToOne(mappedBy = "gerente")
	private Conta conta;

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	
	
}
