package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Repository.MovimentacaoRepository;

@Service
public class MovimentacaoService {

	@Autowired
	private MovimentacaoRepository movimentacaoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Movimentacao salvar(Movimentacao movimentacao) {
		return movimentacaoRepository.save(movimentacao);
	}
	
	private Movimentacao buscarMovimentacao(Integer id) {
		if ( movimentacaoRepository.findById(id).isPresent() )
			return movimentacaoRepository.findById(id).get();
		return null;
	}
	
}
