import java.util.ArrayList;
public class NoturnosPorUltimo implements EstrategiaDeAtaque {
    private OrdenacaoExercito ordenar = new OrdenacaoExercito();
    public OrdenacaoExercito getOrdenar(){
        return this.ordenar;
    }

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> vivos = ordenar.selecionaVivos(atacantes);
        ArrayList<Elfo> verdes = ordenar.selecionaVerdes(vivos);
        ArrayList<Elfo> noturnos = ordenar.selecionaNoturnos(vivos);
        ArrayList<Elfo> ordenado = new ArrayList<Elfo>();
        ordenado.addAll(verdes);
        ordenado.addAll(noturnos);

        return ordenado;
    }

   
}
