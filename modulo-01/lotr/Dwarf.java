public class Dwarf extends Personagem{
    
    private boolean escudoEquipado = false;
    
    public Dwarf(String nome) {
        super(nome);
        this.vida = 110.0;
        this.qtdDanoQueRecebe = 10.0;
        this.itens.adicionarItemNoInventario(new Item(1, "Escudo"));
    }
    
    public boolean getEscudoEquipado() {
        return this.escudoEquipado;
    }
    
    public void equiparEscudo () {
        this.qtdDanoQueRecebe = 5.0;
        this.escudoEquipado = true;
    }
    
}