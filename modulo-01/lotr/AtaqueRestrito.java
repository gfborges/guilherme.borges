import java.util.ArrayList;
public class AtaqueRestrito implements EstrategiaDeAtaque {
    private OrdenacaoExercito ordenar = new OrdenacaoExercito();
    
    public OrdenacaoExercito getOrdenar(){
        return this.ordenar;
    }

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {        
        ArrayList<Elfo> vivosComFlecha = ordenar.selecionaVivosComFlecha(atacantes);
        ArrayList<Elfo> ordenado = ordenar.ordenaPorQtdFlecha(vivosComFlecha);
        
        int qtdAtualNoturnos = ordenar.selecionaNoturnos(ordenado).size();
        int qtdMaxNoturnos = (int) (ordenado.size() * 0.3);
        int i = ordenado.size() - 1;
        while (qtdAtualNoturnos > qtdMaxNoturnos) {
            if (ordenado.get(i) instanceof ElfoNoturno) {
                ordenado.remove(i);
                qtdAtualNoturnos--;
            }
            i--;
        }        
        
        return ordenado;
    }
}
