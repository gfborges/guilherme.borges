

public class PaginadorInventario {
    
    private Inventario inventario;
    private int marcador = 0;
    
    public PaginadorInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public void pular(int para) {
        if (para < 0)
            this.marcador = 0;
        else if (this.marcador + para < this.inventario.getTamanhoInventario()) 
            this.marcador += para;        
    }    
    
    public Inventario limitar(int qtd) {
        Inventario limitado= new Inventario();
        int i = marcador;
        int cont = 0;
                
        while (cont < qtd) {
            if (this.marcador + cont < this.inventario.getTamanhoInventario()) {
                limitado.adicionarItemNoInventario(this.inventario.obtemItemNaPosicao(i));
            }
            i++;
            cont++;
        }
                
        return limitado;
    }
}
