import java.util.ArrayList;

public interface EstrategiaDeAtaque {
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);
    
}
