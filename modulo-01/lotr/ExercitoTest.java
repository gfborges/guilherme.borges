import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoTest {
    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        Exercito exercito = new Exercito();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getExercitoDeElfo().contains(elfoVerde));
    }

    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        Exercito exercito = new Exercito();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getExercitoDeElfo().contains(elfoNoturno));
    }

    @Test
    public void naoPodeAlistarElfo() {
        Elfo elfo = new Elfo("Common Legolas");
        Exercito exercito = new Exercito();
        exercito.alistar(elfo);
        assertFalse(exercito.getExercitoDeElfo().contains(elfo));
    }

    @Test
    public void naoPodeAlistarElfoDaLuz() {
        Elfo elfo = new ElfoDaLuz("Light Legolas");
        Exercito exercito = new Exercito();
        exercito.alistar(elfo);
        assertFalse(exercito.getExercitoDeElfo().contains(elfo));
    }

    @Test
    public void buscarElfosRecemCriadosExistindo() {
        Elfo recemCriado = new ElfoVerde("Galadriel");
        Exercito exercito = new Exercito();
        exercito.alistar(recemCriado);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(recemCriado)
            );
        assertEquals(esperado, exercito.buscarExercito(Status.RECEM_CRIADO));
    }

    @Test
    public void buscarElfosVivosNaoExistindo() {
        Elfo recemCriadoTrocaPraMorto = new ElfoNoturno("Galadriel");
        Exercito exercito = new Exercito();
        // simular morte
        recemCriadoTrocaPraMorto.getFlecha().setQuantidade(7);
        for (int i = 0; i < 7; i++) {
            recemCriadoTrocaPraMorto.atirarFlecha(new Dwarf("Gimli"));
        }
        exercito.alistar(recemCriadoTrocaPraMorto);
        assertNull(exercito.buscarExercito(Status.RECEM_CRIADO));
    }

    @Test
    public void buscarElfosMortosExistindo() {
        Elfo recemCriadoTrocaPraMorto = new ElfoNoturno("Undead Galadriel");
        Exercito exercito = new Exercito();
        // simular morte
        recemCriadoTrocaPraMorto.getFlecha().setQuantidade(7);
        for (int i = 0; i < 7; i++) {
            recemCriadoTrocaPraMorto.atirarFlecha(new Dwarf("Gimli"));
        }
        exercito.alistar(recemCriadoTrocaPraMorto);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(recemCriadoTrocaPraMorto)
            );
        assertEquals(esperado, exercito.buscarExercito(Status.MORTO));
    }    

    @Test
    public void buscarElfosMortosNaoExistindo() {
        Elfo recemCriado = new ElfoVerde("Galadriel");
        Exercito exercito = new Exercito();
        exercito.alistar(recemCriado);
        assertNull(exercito.buscarExercito(Status.MORTO));
    }
}
