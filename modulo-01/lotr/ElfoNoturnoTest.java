

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoNoturnoTest
{
    @Test
    public void elfoNoturnoAtiraFlechaUmaVez() {
        // Arrange
        ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        elfoNoturno.atirarFlecha(umDwarf);
        // Assert
        assertEquals(3, elfoNoturno.getExperiencia());
        assertEquals(1, elfoNoturno.getFlecha().getQuantidade());
        assertEquals(85.0, elfoNoturno.getVida(), 1e-8);
        assertEquals(Status.SOFREU_DANO, elfoNoturno.getStatus());
    }
    
    @Test
    public void elfoNoturnoAtiraFlechaDuasVez() {
        // Arrange
        ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        elfoNoturno.atirarFlecha(umDwarf);
        elfoNoturno.atirarFlecha(umDwarf);
        // Assert
        assertEquals(6, elfoNoturno.getExperiencia());
        assertEquals(0, elfoNoturno.getFlecha().getQuantidade());
        assertEquals(70.0, elfoNoturno.getVida(), 1e-8);
        assertEquals(Status.SOFREU_DANO, elfoNoturno.getStatus());
    }
    
    @Test
    public void elfoNoturnoAtiraFlechaTresVez() {
        // Arrange
        ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        for (int i = 0; i < 3; i++)
            elfoNoturno.atirarFlecha(umDwarf);
        // Assert
        assertEquals(6, elfoNoturno.getExperiencia());
        assertEquals(0, elfoNoturno.getFlecha().getQuantidade());
        assertEquals(70.0, elfoNoturno.getVida(), 1e-8);
        assertEquals(Status.SOFREU_DANO, elfoNoturno.getStatus());
    }
    
    @Test
    public void elfoNoturnoMorreAoAtirarFlecha() {
        // Arrange
        ElfoNoturno elfoNoturno = new ElfoNoturno("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        elfoNoturno.getItens().obtemItemNaPosicao(1).setQuantidade(20);
        // Act
        for (int i = 0; i < 12; i++)
            elfoNoturno.atirarFlecha(umDwarf);
        // Assert
        assertEquals(21, elfoNoturno.getExperiencia());
        assertEquals(13, elfoNoturno.getFlecha().getQuantidade());
        assertEquals(0.0, elfoNoturno.getVida(), 1e-8);
        assertEquals(Status.MORTO, elfoNoturno.getStatus());
    }
}
