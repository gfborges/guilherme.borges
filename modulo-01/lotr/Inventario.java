import java.util.ArrayList;    

public class Inventario {    
    private ArrayList<Item> inventario;    
    
    public Inventario() {
        this.inventario = new ArrayList<Item>();
    }
    
    public int getTamanhoInventario() {
        return inventario.size();
    }
    
    public ArrayList<Item> getInventario() {
        return this.inventario;
    }
    
    public void adicionarItemNoInventario(Item item) {
        this.inventario.add(item);
    }    
    
    public Item obtemItemNaPosicao(int posicao) {
        if (posicao >= this.inventario.size())
            return null;
        return this.inventario.get(posicao);
    }
    
    public void removeItemNaPosicao(int posicao) {
        if (posicao < this.inventario.size())
            this.inventario.remove(posicao);
    }
    
    public void removerItem(Item item) {
        this.inventario.remove(item);
    }
    
    public boolean inventarioVazio() {
        return this.inventario.isEmpty();
    }
    
    public Inventario unirInventario(Inventario inventarioNovo) {
        ArrayList<Item> inventarioUnido = new ArrayList<Item>(this.inventario);        
        
        if (!inventarioNovo.inventarioVazio()) {
            for (Item novo : inventarioNovo.getInventario()) {
                boolean uniu = false;
                for (int i = 0; i < this.inventario.size(); i++)
                    if (this.inventario.get(i).descricoesIguais(novo)) {
                        inventarioUnido.get(i).setQuantidade(inventarioUnido.get(i).getQuantidade()+novo.getQuantidade());
                        uniu = true;
                        break;
                    }
                if (!uniu)
                    inventarioUnido.add(novo);                      
            }
        }
        
        Inventario unido = new Inventario();
        unido.inventario = inventarioUnido;
        return unido;
    }
    
    public Inventario diferenciarInventario(Inventario inventarioNovo) {
        ArrayList<Item> inventarioDiferente = new ArrayList<Item>(this.inventario);        
        
        if (!inventarioNovo.inventarioVazio()) {
            for (Item novo : inventarioNovo.getInventario()) {
                for (int i = 0; i < this.inventario.size(); i++)                    
                    if (this.inventario.get(i).descricoesIguais(novo)) {
                        inventarioDiferente.remove(this.inventario.get(i));
                        break;
                    }
            }
        }
                
        Inventario diferente = new Inventario();
        diferente.inventario = inventarioDiferente;
        return diferente;
    }
    
    public Inventario cruzarInventario(Inventario inventarioNovo) {
        ArrayList<Item> inventarioCruzado = new ArrayList<Item>();        
        
        if (!inventarioNovo.inventarioVazio()) {
            for (Item novo : inventarioNovo.getInventario()) {
                for (int i = 0; i < this.inventario.size(); i++)
                    if (this.inventario.get(i).descricoesIguais(novo)) {
                        inventarioCruzado.add(this.inventario.get(i));
                        break;
                    }
            }
        }
                
        Inventario cruzado = new Inventario();
        cruzado.inventario = inventarioCruzado;
        return cruzado;
    }
    
    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder();
        
        for (int i = 0; i < this.inventario.size(); i++) {
            if (this.inventario.get(i) != null) {
                String descricao = this.inventario.get(i).getDescricao();
                descricoes.append(descricao);
                boolean deveColocarVirgula = i < this.inventario.size() - 1;
                if (deveColocarVirgula) {
                    descricoes.append(",");
                }
            }
        }
        
        return descricoes.toString();
    }
        
    public Item itemComMaiorQuantidade() {
        this.ordenarInventario();
        return this.inventario.get(inventario.size()-1);
    }
    
    public Item buscarPorDescricao(String descricao) {
        Item procurado = null;
        boolean encontrou = false;
        
        for (int i = 0; i < inventario.size() && !encontrou; i++) {
            if (inventario.get(i).getDescricao().equalsIgnoreCase(descricao)) {
                procurado = inventario.get(i);
                encontrou = true;
            }                
        }
        
        return procurado;
    }
    
    public void ordenarInventario() {
        if (inventario.size() >= 2) {        
            Item menor = inventario.get(0);
            if (inventario.get(1).getQuantidade() < menor.getQuantidade())
                menor = inventario.get(1);
                            
            ArrayList<Item> ordenado = new ArrayList<Item>();
            int i = 1;
                            
            while (inventario.size() >=2) {
                while (i < inventario.size()) {
                if (inventario.get(i).getQuantidade() < menor.getQuantidade()) {
                   menor = inventario.get(i);
                }
                i++;
                }
                i = 1;
                ordenado.add(menor);
                inventario.remove(menor);
                menor = inventario.get(0);
            }
            
            ordenado.add(inventario.get(0));
                    
            this.inventario = new ArrayList<Item>(ordenado);
        }
    }
    
    public void ordenarInventario(TipoOrdenacao ordem) {
        if (inventario.size() >= 2) {
            if (ordem == TipoOrdenacao.ASC) {
                this.ordenarInventario();              
            }
            if (ordem == TipoOrdenacao.DESC) {
                this.ordenarInventario();
                                                
                ArrayList<Item> ordenado = new ArrayList<Item>();
                
                for (int i = inventario.size()-1; i >= 0; i--) {
                    ordenado.add(inventario.get(i));
                }
                
                this.inventario = new ArrayList<Item>(ordenado);
            }            
        }
    }
    
    public ArrayList<Item> inverterInventario() {
        ArrayList<Item> invertido = new ArrayList<Item>();
        
        for (int i = inventario.size()-1; i >= 0; i--) {
                    invertido.add(inventario.get(i));
        }
        
        return invertido;
    }    
    
}
