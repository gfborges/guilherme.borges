

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest
{
    @Test
    public void verificaQtdInicialVidaDwarf() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        assertEquals(110.0, umDwarf.getVida(), 1e-8);
    }
    
    @Test
    public void dwarfNasceComEscudoSemEquipar() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        assertEquals("Escudo", umDwarf.getItens().getDescricoesItens());
        assertFalse(umDwarf.getEscudoEquipado());
    }
    
    @Test
    public void dwarfPerdeVidaComEscudoEquipado() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        umDwarf.equiparEscudo();
        umDwarf.sofrerDano();
        assertEquals(105.0, umDwarf.getVida(), 1e-8);
    }
    
    @Test
    public void dwarfPerdeVidaSemEscudoEquipado() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        umDwarf.sofrerDano();
        assertEquals(100.0, umDwarf.getVida(), 1e-8);
    }
    
    @Test
    public void dwarfMorreComEscudoEquipado() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        umDwarf.equiparEscudo();
        
        for (int i = 0; i < 23; i++)
            umDwarf.sofrerDano();
        
        assertEquals(Status.MORTO, umDwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeDuasVezesSemEscudoEquipado() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        umDwarf.sofrerDano();
        umDwarf.sofrerDano();
        assertEquals(90.0, umDwarf.getVida(), 1e-8);
    }
    
    @Test
    public void dwarfPerdeDuasVezesComEscudoEquipado() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        umDwarf.equiparEscudo();
        umDwarf.sofrerDano();
        umDwarf.sofrerDano();
        assertEquals(100.0, umDwarf.getVida(), 1e-8);
    }
    
    @Test
    public void dwarfMorreQuandoVidaZera() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        for (int i = 0; i < 13; i++)
            umDwarf.sofrerDano();
        assertEquals(00.0, umDwarf.getVida(), 1e-8);
        assertEquals(Status.MORTO, umDwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeVidaEContinuaVivo() {
        Dwarf umDwarf = new Dwarf("Bad Guy");
        umDwarf.sofrerDano();
        umDwarf.sofrerDano();
        assertEquals(Status.SOFREU_DANO, umDwarf.getStatus());
    }
    
    @Test
    public void dwarfNasceComStatusRecemCriado() {
         Dwarf umDwarf = new Dwarf("Bad Guy");
        assertEquals(Status.RECEM_CRIADO, umDwarf.getStatus());
    }
}
