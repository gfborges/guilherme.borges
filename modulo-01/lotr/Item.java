public class Item {
    
    protected int quantidade;
    protected String descricao;
    
    public Item (int qtd, String desc) {
        this.setQuantidade(qtd);
        this.descricao = desc;
    }
    
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    public boolean descricoesIguais(Item item) {
        return this.descricao.equalsIgnoreCase(item.descricao);
    }
    
    public void setQuantidade(int qtd) {
        this.quantidade = qtd;
    }
    
    public boolean equals(Object obj) {
        Item outroItem = (Item)obj;
        return this.quantidade == outroItem.getQuantidade() && this.descricao.equalsIgnoreCase(outroItem.getDescricao());
    }

}