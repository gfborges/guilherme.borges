
public class ElfoNoturno extends Elfo {
    
    public ElfoNoturno(String nome) {
        super(nome);
        this.qtdDanoQueRecebe = 15.0;
        this.qtdExpQueRecebe = 3;
    }    
}
