
public abstract class Personagem {
    
    protected String nome;
    protected Status status = Status.RECEM_CRIADO;
    protected Inventario itens = new Inventario();
    protected double vida;
    protected double qtdDanoQueRecebe;
    
    public Personagem(String nome) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
        
    public Status getStatus() {
        return this.status;
    }    
    
    public Inventario getItens() {
        return this.itens;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public double getQtdDanoQueRecebe() {
        return this.qtdDanoQueRecebe;
    }
    
    protected boolean possuiVida() {
        return this.vida > 0;
    }
    
    protected void sofrerDano() {
        if (this.possuiVida()) {
            if (this.vida - this.qtdDanoQueRecebe > 0) {
                this.vida -= this.qtdDanoQueRecebe;
                this.status = Status.SOFREU_DANO;            
            }
            else {
                this.vida = 0.0;
                this.status = Status.MORTO;            
            }
        }
    }
    
    public void ganharItem(Item item) {
        this.itens.adicionarItemNoInventario(item);
    }
    
    public void perderItem(Item item) {
        this.itens.removerItem(item);
    }
}
