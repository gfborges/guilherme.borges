import java.util.*;

public class Exercito {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(Arrays.asList(ElfoVerde.class, ElfoNoturno.class));
    private ArrayList<Elfo> exercitoDeElfo = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
    public ArrayList<Elfo> getExercitoDeElfo() {
        return this.exercitoDeElfo;
    }
    
    public void alistar(Elfo elfo) {
        if (TIPOS_PERMITIDOS.contains(elfo.getClass())) {
            this.exercitoDeElfo.add(elfo);
            ArrayList<Elfo> elfosDoStatus = porStatus.get(elfo.getStatus());
            if (elfosDoStatus == null) {
                elfosDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfosDoStatus);
            }
            elfosDoStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo> buscarExercito(Status status) {
        return this.porStatus.get(status);
    }
}
