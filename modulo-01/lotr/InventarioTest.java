

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class InventarioTest {
        
    @Test
    public void iniciaInventarioVazio () {
        Inventario inventario = new Inventario();
        assertEquals(0, inventario.getTamanhoInventario());
    }
        
    @Test
    public void adicionaItemNoPrimeiroSlotNull() {
        Inventario umInventario = new Inventario();
        Item arco = new Item(1, "Arco");
        
        umInventario.adicionarItemNoInventario(arco);
        
        assertEquals(arco, umInventario.obtemItemNaPosicao(0));
    }
        
    @Test
    public void obtemItemNaPosicaoDefinida() {
        Inventario umInventario = new Inventario();
        Item arco = new Item(1, "Arco");
        Item escudo = new Item(1, "Escudo");
        Item bracelete = new Item(1, "Bracelete");
        
        umInventario.adicionarItemNoInventario(arco);
        umInventario.adicionarItemNoInventario(escudo);
        umInventario.adicionarItemNoInventario(bracelete);
                
        assertEquals(escudo, umInventario.obtemItemNaPosicao(1));
    }
    
    
    @Test
    public void removerItemNaPosicao() {
        Inventario umInventario = new Inventario();
        Item arco = new Item(1, "Arco");
        Item escudo = new Item(1, "Escudo");
        Item bracelete = new Item(1, "Bracelete");
                
        umInventario.adicionarItemNoInventario(arco);
        umInventario.adicionarItemNoInventario(escudo);
        umInventario.adicionarItemNoInventario(bracelete);
        umInventario.removeItemNaPosicao(1);
        
        assertEquals(bracelete, umInventario.obtemItemNaPosicao(1));
    }
    
    @Test
    public void removerItemEmPosicaoInvalida() {
        Inventario umInventario = new Inventario();
        Item arco = new Item(1, "Arco");
        Item escudo = new Item(1, "Escudo");
        Item bracelete = new Item(1, "Bracelete");
                
        umInventario.adicionarItemNoInventario(arco);
        umInventario.adicionarItemNoInventario(escudo);
        umInventario.adicionarItemNoInventario(bracelete);
        umInventario.removeItemNaPosicao(5);
        
        assertEquals(bracelete, umInventario.obtemItemNaPosicao(2));
    }
    
    @Test
    public void unirDoisInventariosVazios() {
        Inventario antigo = new Inventario();
        Inventario novo = new Inventario();
        assertTrue(antigo.unirInventario(novo).getInventario().isEmpty());
    }
    
    @Test
    public void unirDoisInventarios() {
        Inventario antigo = new Inventario();        
        antigo.adicionarItemNoInventario(new Item(2, "Adaga"));
        antigo.adicionarItemNoInventario(new Item(5, "Escudo"));
        antigo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        antigo.adicionarItemNoInventario(new Item(15, "Capa"));
        antigo.adicionarItemNoInventario(new Item(3, "Bota"));
        antigo.adicionarItemNoInventario(new Item(8, "Corda"));
        
        Inventario novo = new Inventario();        
        novo.adicionarItemNoInventario(new Item(10, "Bolsa"));
        novo.adicionarItemNoInventario(new Item(3, "Corda"));
        novo.adicionarItemNoInventario(new Item(4, "Adaga"));
        novo.adicionarItemNoInventario(new Item(7, "Faca"));
        
        Inventario unido = new Inventario();
        unido.adicionarItemNoInventario(new Item(6, "Adaga"));
        unido.adicionarItemNoInventario(new Item(5, "Escudo"));
        unido.adicionarItemNoInventario(new Item(4, "Bracelete"));
        unido.adicionarItemNoInventario(new Item(15, "Capa"));
        unido.adicionarItemNoInventario(new Item(3, "Bota"));
        unido.adicionarItemNoInventario(new Item(11, "Corda"));
        unido.adicionarItemNoInventario(new Item(10, "Bolsa"));
        unido.adicionarItemNoInventario(new Item(7, "Faca"));
               
        assertEquals(unido.getInventario(), antigo.unirInventario(novo).getInventario());
    }
    
    @Test
    public void unirDoisInventariosIguais() {
        Inventario antigo = new Inventario();        
        antigo.adicionarItemNoInventario(new Item(2, "Adaga"));
        antigo.adicionarItemNoInventario(new Item(5, "Escudo"));
        antigo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        Inventario novo = new Inventario();        
        novo.adicionarItemNoInventario(new Item(2, "Adaga"));
        novo.adicionarItemNoInventario(new Item(5, "Escudo"));
        novo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        
        Inventario unido = new Inventario();
        unido.adicionarItemNoInventario(new Item(4, "Adaga"));
        unido.adicionarItemNoInventario(new Item(10, "Escudo"));
        unido.adicionarItemNoInventario(new Item(8, "Bracelete"));
               
        assertEquals(unido.getInventario(), antigo.unirInventario(novo).getInventario());
    }
    
    @Test
    public void diferenciarDoisInventariosVazios() {
        Inventario antigo = new Inventario();
        Inventario novo = new Inventario();
        assertTrue(antigo.diferenciarInventario(novo).getInventario().isEmpty());
    }
    
    @Test
    public void diferenciarDoisInventarios() {
        Inventario antigo = new Inventario();        
        antigo.adicionarItemNoInventario(new Item(2, "Adaga"));
        antigo.adicionarItemNoInventario(new Item(5, "Escudo"));
        antigo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        antigo.adicionarItemNoInventario(new Item(15, "Capa"));
        antigo.adicionarItemNoInventario(new Item(3, "Bota"));
        antigo.adicionarItemNoInventario(new Item(8, "Corda"));
        
        Inventario novo = new Inventario();        
        novo.adicionarItemNoInventario(new Item(10, "Bolsa"));
        novo.adicionarItemNoInventario(new Item(3, "Corda"));
        novo.adicionarItemNoInventario(new Item(4, "Adaga"));
        novo.adicionarItemNoInventario(new Item(7, "Faca"));
        
        Inventario diferente = new Inventario();
        diferente.adicionarItemNoInventario(new Item(5, "Escudo"));
        diferente.adicionarItemNoInventario(new Item(4, "Bracelete"));
        diferente.adicionarItemNoInventario(new Item(15, "Capa"));
        diferente.adicionarItemNoInventario(new Item(3, "Bota"));
               
        assertEquals(diferente.getInventario(), antigo.diferenciarInventario(novo).getInventario());
    }
    
    @Test
    public void diferenciarDoisInventariosIguais() {
        Inventario antigo = new Inventario();        
        antigo.adicionarItemNoInventario(new Item(2, "Adaga"));
        antigo.adicionarItemNoInventario(new Item(5, "Escudo"));
        antigo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        
        Inventario novo = new Inventario();        
        novo.adicionarItemNoInventario(new Item(2, "Adaga"));
        novo.adicionarItemNoInventario(new Item(5, "Escudo"));
        novo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        assertTrue(antigo.diferenciarInventario(novo).getInventario().isEmpty());
    }
    
    @Test
    public void cruzarDoisInventariosVazios() {
        Inventario antigo = new Inventario();
        Inventario novo = new Inventario();
        assertTrue(antigo.cruzarInventario(novo).getInventario().isEmpty());
    }
    
    @Test
    public void cruzarDoisInventariosIguais() {
        Inventario antigo = new Inventario();        
        antigo.adicionarItemNoInventario(new Item(2, "Adaga"));
        antigo.adicionarItemNoInventario(new Item(5, "Escudo"));
        antigo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        
        Inventario novo = new Inventario();        
        novo.adicionarItemNoInventario(new Item(4, "Adaga"));
        novo.adicionarItemNoInventario(new Item(10, "Escudo"));
        novo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        assertEquals(antigo.getInventario(), antigo.cruzarInventario(novo).getInventario());
    }
    
    @Test
    public void cruzarDoisInventariosTotalmenteDiferentes() {
        Inventario antigo = new Inventario();        
        antigo.adicionarItemNoInventario(new Item(2, "Adaga"));
        antigo.adicionarItemNoInventario(new Item(5, "Escudo"));
        antigo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        
        Inventario novo = new Inventario();        
        novo.adicionarItemNoInventario(new Item(4, "Capa"));
        novo.adicionarItemNoInventario(new Item(10, "Faca"));
        novo.adicionarItemNoInventario(new Item(4, "Colete"));
        assertTrue(antigo.cruzarInventario(novo).getInventario().isEmpty());
    }
    
    @Test
    public void cruzarDoisInventarios() {
        Inventario antigo = new Inventario();        
        antigo.adicionarItemNoInventario(new Item(2, "Adaga"));
        antigo.adicionarItemNoInventario(new Item(5, "Escudo"));
        antigo.adicionarItemNoInventario(new Item(4, "Bracelete"));
        antigo.adicionarItemNoInventario(new Item(15, "Capa"));
        antigo.adicionarItemNoInventario(new Item(3, "Bota"));
        antigo.adicionarItemNoInventario(new Item(8, "Corda"));
        
        Inventario novo = new Inventario();        
        novo.adicionarItemNoInventario(new Item(10, "Bolsa"));
        novo.adicionarItemNoInventario(new Item(3, "Corda"));
        novo.adicionarItemNoInventario(new Item(4, "Adaga"));
        novo.adicionarItemNoInventario(new Item(7, "Faca"));
        
        Inventario cruzado = new Inventario();
        cruzado.adicionarItemNoInventario(new Item(8, "Corda"));
        cruzado.adicionarItemNoInventario(new Item(2, "Adaga"));
               
        assertEquals(cruzado.getInventario(), antigo.cruzarInventario(novo).getInventario());
    }
    
    @Test
    public void retornaStringComDescricaoItens() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(1, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(1, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(1, "Bracelete"));
        
        assertEquals("Adaga,Escudo,Bracelete", umInventario.getDescricoesItens());
    }
    
    @Test
    public void getDescricaoComInventarioVazio() {
        Inventario umInventario = new Inventario();
        
        assertEquals("", umInventario.getDescricoesItens());
    }
                   
    @Test
    public void obtemTamanhoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(1, "Bracelete"));
        
        assertEquals(3, umInventario.getTamanhoInventario());
    }
    
    @Test
    public void itemComMaiorQuantidade() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
               
        assertEquals(15, umInventario.itemComMaiorQuantidade().getQuantidade());
    }
    
    @Test
    public void ordenaInventarioVazio() {
        Inventario umInventario = new Inventario();
        umInventario.ordenarInventario();
        assertTrue(umInventario.inventarioVazio());
    }
    
    @Test
    public void ordenaInventarioComUmItem() {
        Inventario umInventario = new Inventario();
        Item espada = new Item(1, "Espada");
        umInventario.adicionarItemNoInventario(espada);
        umInventario.ordenarInventario();
        ArrayList<Item> esperado = new ArrayList<Item>();
        esperado.add(espada);
        assertEquals(esperado, umInventario.getInventario());        
    }    
    
    @Test
    public void ordenaInventarioPorQuantidadeCrescente() {
        Inventario umInventario = new Inventario();
        
        Item a = new Item(2, "Adaga");
        Item b = new Item(5, "Adaga");
        Item c = new Item(8, "Adaga");
        Item d = new Item(1, "Adaga");
        Item e = new Item(15, "Adaga");
        Item f = new Item(6, "Adaga");
        Item g = new Item(10, "Adaga");
        Item h = new Item(11, "Adaga");
        
        umInventario.adicionarItemNoInventario(a);
        umInventario.adicionarItemNoInventario(b);
        umInventario.adicionarItemNoInventario(c);
        umInventario.adicionarItemNoInventario(d);
        umInventario.adicionarItemNoInventario(e);
        umInventario.adicionarItemNoInventario(f);
        umInventario.adicionarItemNoInventario(g);
        umInventario.adicionarItemNoInventario(h);
        
        umInventario.ordenarInventario();
                      
        assertEquals(1, umInventario.obtemItemNaPosicao(0).getQuantidade());
        assertEquals(2, umInventario.obtemItemNaPosicao(1).getQuantidade());
        assertEquals(5, umInventario.obtemItemNaPosicao(2).getQuantidade());
        assertEquals(6, umInventario.obtemItemNaPosicao(3).getQuantidade());
        assertEquals(8, umInventario.obtemItemNaPosicao(4).getQuantidade());
        assertEquals(10, umInventario.obtemItemNaPosicao(5).getQuantidade());
        assertEquals(11, umInventario.obtemItemNaPosicao(6).getQuantidade());
        assertEquals(15, umInventario.obtemItemNaPosicao(7).getQuantidade());              
    }
    
    @Test
    public void ordenaInventarioInformandoOrdem() {
        Inventario umInventario = new Inventario();
        
        Item a = new Item(2, "Adaga");
        Item b = new Item(5, "Adaga");
        Item c = new Item(8, "Adaga");
        Item d = new Item(1, "Adaga");
        Item e = new Item(15, "Adaga");
        Item f = new Item(6, "Adaga");
        Item g = new Item(10, "Adaga");
        Item h = new Item(11, "Adaga");
        
        umInventario.adicionarItemNoInventario(a);
        umInventario.adicionarItemNoInventario(b);
        umInventario.adicionarItemNoInventario(c);
        umInventario.adicionarItemNoInventario(d);
        umInventario.adicionarItemNoInventario(e);
        umInventario.adicionarItemNoInventario(f);
        umInventario.adicionarItemNoInventario(g);
        umInventario.adicionarItemNoInventario(h);
        
        umInventario.ordenarInventario(TipoOrdenacao.DESC);
                      
        assertEquals(15, umInventario.obtemItemNaPosicao(0).getQuantidade());
        assertEquals(11, umInventario.obtemItemNaPosicao(1).getQuantidade());
        assertEquals(10, umInventario.obtemItemNaPosicao(2).getQuantidade());
        assertEquals(8, umInventario.obtemItemNaPosicao(3).getQuantidade());
        assertEquals(6, umInventario.obtemItemNaPosicao(4).getQuantidade());
        assertEquals(5, umInventario.obtemItemNaPosicao(5).getQuantidade());
        assertEquals(2, umInventario.obtemItemNaPosicao(6).getQuantidade());
        assertEquals(1, umInventario.obtemItemNaPosicao(7).getQuantidade());              
    }
    
     @Test
    public void ordenaInventarioJaOrdenado() {
        Inventario umInventario = new Inventario();
        
        Item a = new Item(2, "Adaga");
        Item b = new Item(5, "Adaga");
        Item c = new Item(8, "Adaga");
        Item d = new Item(1, "Adaga");
        Item e = new Item(15, "Adaga");
        Item f = new Item(6, "Adaga");
        Item g = new Item(10, "Adaga");
        Item h = new Item(11, "Adaga");
        
        umInventario.adicionarItemNoInventario(d);
        umInventario.adicionarItemNoInventario(a);
        umInventario.adicionarItemNoInventario(b);
        umInventario.adicionarItemNoInventario(f);
        umInventario.adicionarItemNoInventario(c);
        umInventario.adicionarItemNoInventario(g);
        umInventario.adicionarItemNoInventario(h);
        umInventario.adicionarItemNoInventario(e);
        
        umInventario.ordenarInventario();
                      
        assertEquals(1, umInventario.obtemItemNaPosicao(0).getQuantidade());
        assertEquals(2, umInventario.obtemItemNaPosicao(1).getQuantidade());
        assertEquals(5, umInventario.obtemItemNaPosicao(2).getQuantidade());
        assertEquals(6, umInventario.obtemItemNaPosicao(3).getQuantidade());
        assertEquals(8, umInventario.obtemItemNaPosicao(4).getQuantidade());
        assertEquals(10, umInventario.obtemItemNaPosicao(5).getQuantidade());
        assertEquals(11, umInventario.obtemItemNaPosicao(6).getQuantidade());
        assertEquals(15, umInventario.obtemItemNaPosicao(7).getQuantidade());              
    }
    
    @Test
    public void ordenarDescInventarioVazio() {
        Inventario umInventario = new Inventario();
        umInventario.ordenarInventario(TipoOrdenacao.DESC);
        assertTrue(umInventario.inventarioVazio());
    }
    
    @Test
    public void ordenarDescInventarioComUmItem() {
        Inventario umInventario = new Inventario();
        Item espada = new Item(1, "Espada");
        umInventario.adicionarItemNoInventario(espada);
        umInventario.ordenarInventario(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<Item>();
        esperado.add(espada);
        assertEquals(esperado, umInventario.getInventario());        
    }    
    
    @Test
    public void ordenaInventarioOrdenadoAoContrario() {
        Inventario umInventario = new Inventario();
        
        Item a = new Item(2, "Adaga");
        Item b = new Item(5, "Adaga");
        Item c = new Item(8, "Adaga");
        Item d = new Item(1, "Adaga");
        Item e = new Item(15, "Adaga");
        Item f = new Item(6, "Adaga");
        Item g = new Item(10, "Adaga");
        Item h = new Item(11, "Adaga");
        
        umInventario.adicionarItemNoInventario(d);
        umInventario.adicionarItemNoInventario(a);
        umInventario.adicionarItemNoInventario(b);
        umInventario.adicionarItemNoInventario(f);
        umInventario.adicionarItemNoInventario(c);
        umInventario.adicionarItemNoInventario(g);
        umInventario.adicionarItemNoInventario(h);
        umInventario.adicionarItemNoInventario(e);
        
        umInventario.ordenarInventario(TipoOrdenacao.DESC);
        
        assertEquals(15, umInventario.obtemItemNaPosicao(0).getQuantidade());
        assertEquals(11, umInventario.obtemItemNaPosicao(1).getQuantidade());
        assertEquals(10, umInventario.obtemItemNaPosicao(2).getQuantidade());
        assertEquals(8, umInventario.obtemItemNaPosicao(3).getQuantidade());
        assertEquals(6, umInventario.obtemItemNaPosicao(4).getQuantidade());
        assertEquals(5, umInventario.obtemItemNaPosicao(5).getQuantidade());
        assertEquals(2, umInventario.obtemItemNaPosicao(6).getQuantidade());
        assertEquals(1, umInventario.obtemItemNaPosicao(7).getQuantidade());                 
    }
    
    @Test
    public void procuraItemPorDescricao() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
                
        assertEquals("Capa", umInventario.buscarPorDescricao("Capa").getDescricao());
    }
    
    @Test
    public void procuraItemPorDescricaoComInventarioVazio() {
        Inventario umInventario = new Inventario();
                
        assertNull(umInventario.buscarPorDescricao("Capa"));
    }
    
    @Test
    public void procuraItemPorDescricaoForaDoInventario() {
        Inventario umInventario = new Inventario();
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
                
        assertNull(umInventario.buscarPorDescricao("Capa"));
    }
    
    @Test
    public void procuraItemPorDescricaoNaPrimeiraPosicao() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
                
        assertEquals("Adaga", umInventario.buscarPorDescricao("Adaga").getDescricao());
    }
    
    @Test
    public void procuraItemPorDescricaoNaUltimaPosicao() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
                
        assertEquals("Corda", umInventario.buscarPorDescricao("Corda").getDescricao());
    }
    
    @Test
    public void inverteInventarioVazio() {
        Inventario umInventario = new Inventario();
                        
        assertTrue (umInventario.inverterInventario().isEmpty());
    }
            
    @Test
    public void inverteOrdemDoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(1, "Bracelete"));
                        
        assertEquals("Bracelete", umInventario.inverterInventario().get(0).getDescricao());
        assertEquals("Escudo", umInventario.inverterInventario().get(1).getDescricao());
        assertEquals("Adaga", umInventario.inverterInventario().get(2).getDescricao());
    }
    
    
    
    
}