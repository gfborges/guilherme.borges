import java.util.Random;

public class DadoD6 implements Sorteador {
    private Random gerador = new Random(6);
    
    public void setSeedGerador(int seed) {
        this.gerador.setSeed(seed);
    }
    
    public int sortear() {
        return this.gerador.nextInt(6) + 1;
    }
}
