

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PaginadorInventarioTest
{
    @Test
    public void pularLimitarInventarioVazio() {
        Inventario umInventario = new Inventario();        
        PaginadorInventario paginador = new PaginadorInventario(umInventario);        
        
        assertTrue(paginador.limitar(3).inventarioVazio());
    }
    
    @Test
    public void pularLimitarComApenasUmItem() {
        Inventario umInventario = new Inventario();
        Item adaga = new Item(2, "Adaga");
        umInventario.adicionarItemNoInventario(adaga);
        PaginadorInventario paginador = new PaginadorInventario(umInventario);        
        
        assertEquals(adaga, paginador.limitar(3).obtemItemNaPosicao(0));
        assertEquals(1, paginador.limitar(3).getTamanhoInventario());
    }
    
    @Test
    public void limitaDoisItensNoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
        
        PaginadorInventario paginador = new PaginadorInventario(umInventario);
                
        assertEquals(3, paginador.limitar(3).getTamanhoInventario());
    }
    
    @Test
    public void segundaPaginaDeDoisItensNoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
        
        PaginadorInventario paginador = new PaginadorInventario(umInventario);
        paginador.pular(2);
        
        assertEquals("Bracelete,Capa", paginador.limitar(2).getDescricoesItens());
    }
    
    @Test
    public void ultimaPaginaComMenosItens() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
        
        PaginadorInventario paginador = new PaginadorInventario(umInventario);
        paginador.pular(4);
        
        assertEquals("Bota,Corda", paginador.limitar(4).getDescricoesItens());
    }
    
    @Test
    public void pularNegativo() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
                
        PaginadorInventario paginador = new PaginadorInventario(umInventario);
        paginador.pular(-2);
                
        assertEquals(3, paginador.limitar(3).getTamanhoInventario());
    }
}
