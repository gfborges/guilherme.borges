

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest
{
    @Test
    public void elfoVerdeAtiraFlechaUmaVez() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        elfoVerde.atirarFlecha(umDwarf);
        // Assert
        assertEquals(2, elfoVerde.getExperiencia());
        assertEquals(1, elfoVerde.getFlecha().getQuantidade());
    }
    
    @Test
    public void elfoVerdeAtiraFlechaDuasVez() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        elfoVerde.atirarFlecha(umDwarf);
        elfoVerde.atirarFlecha(umDwarf);
        // Assert
        assertEquals(4, elfoVerde.getExperiencia());
        assertEquals(0, elfoVerde.getFlecha().getQuantidade());
    }
    
    @Test
    public void elfoVerdeAtiraFlechaTresVez() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        elfoVerde.atirarFlecha(umDwarf);
        elfoVerde.atirarFlecha(umDwarf);
        elfoVerde.atirarFlecha(umDwarf);
        // Assert
        assertEquals(4, elfoVerde.getExperiencia());
        assertEquals(0, elfoVerde.getFlecha().getQuantidade());
    }
    
    @Test
    public void elfoVerdeGanhaUmItemValido() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        
        elfoVerde.ganharItem(arcoVidro);
        
        assertEquals(arcoVidro, elfoVerde.getItens().obtemItemNaPosicao(2));
    }
    
    @Test
    public void elfoVerdeGanhaUmItemInvalido() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Item vidro = new Item(1, "Vidro");
        
        elfoVerde.ganharItem(vidro);
        
        assertNull(elfoVerde.getItens().obtemItemNaPosicao(2));
        assertEquals(2, elfoVerde.getItens().getTamanhoInventario());
    }
    
    @Test
    public void elfoVerdeGanhaUmItemInvalidoEUmValido() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Item vidro = new Item(1, "Vidro");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        
        elfoVerde.ganharItem(vidro);
        elfoVerde.ganharItem(arcoVidro);
        
        assertNull(elfoVerde.getItens().obtemItemNaPosicao(3));
        assertEquals(arcoVidro, elfoVerde.getItens().obtemItemNaPosicao(2));
        assertEquals(3, elfoVerde.getItens().getTamanhoInventario());
    }
    
    @Test
    public void elfoVerdeGanhaDoisItemValidos() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        Item flechaVidro = new Item(1, "Flecha de Vidro");
        
        elfoVerde.ganharItem(arcoVidro);
        elfoVerde.ganharItem(flechaVidro);
        
        assertNull(elfoVerde.getItens().obtemItemNaPosicao(4));
        assertEquals(4, elfoVerde.getItens().getTamanhoInventario());
        assertEquals(arcoVidro, elfoVerde.getItens().obtemItemNaPosicao(2));
        assertEquals(flechaVidro, elfoVerde.getItens().obtemItemNaPosicao(3));
    }
    
    @Test
    public void elfoVerdePerdeUmItemValido() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        
        elfoVerde.ganharItem(arcoVidro);
        elfoVerde.perderItem(arcoVidro);
        
        assertNull(elfoVerde.getItens().obtemItemNaPosicao(2));
    }
    
    @Test
    public void elfoVerdePerdeUmItemInvalido() {
        // Arrange
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        Item arco = new Item(1, "Arco");
        
        elfoVerde.ganharItem(arcoVidro);
        elfoVerde.perderItem(arco);
        
        assertEquals(3, elfoVerde.getItens().getTamanhoInventario());
        assertEquals(arcoVidro, elfoVerde.getItens().obtemItemNaPosicao(2));
        assertNull(elfoVerde.getItens().obtemItemNaPosicao(3));        
    }
     
}
