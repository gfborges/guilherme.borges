

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemPermanenteTest
{
    @Test
    public void itemPermanenteNaoPodeSerZerado() {
        Item item = new ItemPermanente(1, "Anduril");
        item.setQuantidade(0);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemPermanenteNaoPodeSerNegativo() {
        Item item = new ItemPermanente(1, "Anduril");
        item.setQuantidade(-10);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemPermanentePodeReceberOutraQuantidade() {
        Item item = new ItemPermanente(2, "Escudo de madeira");
        item.setQuantidade(3);
        assertEquals(3, item.getQuantidade());
    }

    @Test
    public void itemPermanenteNaoPodeSerCriadoComZero() {
        Item item = new ItemPermanente(0, "Escudo de madeira");
        assertEquals(1, item.getQuantidade());
    }
}
