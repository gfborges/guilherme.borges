import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerde extends Elfo {
    
    private static final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<> (Arrays.asList("Espada de aço valiriano",
    "Arco de Vidro", "Flecha de Vidro"));
    
    public ElfoVerde(String nome) {
        super(nome);
        this.qtdExpQueRecebe = 2;
    }
    
    private boolean validarItem(Item item) {
        return this.DESCRICOES_VALIDAS.contains(item.getDescricao());
    }
    
    public void ganharItem(Item item) {
        if (validarItem(item))
            this.itens.adicionarItemNoInventario(item);
    }
    
    public void perderItem(Item item) {
        if (validarItem(item))
            this.itens.removerItem(item);
    }
}
