import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test {
    @Test
    public void sortearEntre1E6() {
        DadoD6 dado = new DadoD6();
        boolean numCorreto = true;
    
        for (int i = 0; i < 50; i++)
        if (dado.sortear() > 6 || 1 > dado.sortear())
            numCorreto = false;
        
        assertTrue(numCorreto);   
    }
    
    @Test
    public void jogarDoisDadosComMesmaSeedGeraMesmoNumero() {
        DadoD6 dado1 = new DadoD6();
        dado1.setSeedGerador(3);

        DadoD6 dado2 = new DadoD6();
        dado2.setSeedGerador(3);

        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
    }
    
    @Test
    public void jogarDoisDadosComSeedsDiferentesGeraNumerosDiferentes() {
        DadoD6 dado1 = new DadoD6();
        dado1.setSeedGerador(3);

        DadoD6 dado2 = new DadoD6();
        dado2.setSeedGerador(3000);

        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
    }
}
