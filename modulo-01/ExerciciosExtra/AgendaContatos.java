import java.util.HashMap;
import java.util.Map;

public class AgendaContatos {
    private HashMap<String, String> agenda;
    
    public AgendaContatos() {
        this.agenda = new HashMap<String, String>();
    }
    
    public HashMap<String, String> getAgenda() {
        return this.agenda;
    }
    
    public boolean agendaVazia() {
        return this.agenda.isEmpty();
    }
    
    public int tamanhoAgenda() {
        return this.agenda.size();
    }
    
    public void adicionarContato(String nome, String telefone) {
        this.agenda.put(nome, telefone);
    }
    
    public String buscarTelefone(String nome) {
        return this.agenda.get(nome);        
    }
    
    public String buscarNome(String telefone) {
        for (Map.Entry <String, String> contatos : this.agenda.entrySet())
            if (contatos.getValue().equals(telefone))
                return contatos.getKey();
        return null;
    }  
}
