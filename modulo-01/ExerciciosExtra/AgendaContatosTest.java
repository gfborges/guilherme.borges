import java.util.HashMap;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest {
    @Test
    public void instanciarAgendaVazia() {
        AgendaContatos mapa = new AgendaContatos();
        assertTrue(mapa.agendaVazia());
    }
    
    @Test
    public void adicionarContato() {
        AgendaContatos mapa = new AgendaContatos();
        mapa.adicionarContato("Bernardo", "555555");
        assertEquals(1, mapa.tamanhoAgenda());
    }
    
    @Test
    public void adicionarDoisContatos() {
        AgendaContatos mapa = new AgendaContatos();
        mapa.adicionarContato("Bernardo", "555555");
        mapa.adicionarContato("VemSer", "111111");
        assertEquals(2, mapa.tamanhoAgenda());
    }
    
    @Test
    public void buscarTelefoneEmAgendaVazia() {
        AgendaContatos mapa = new AgendaContatos();
        assertNull(mapa.buscarTelefone("Bernardo"));        
    }
    
    @Test
    public void buscarTelefoneValido() {
        AgendaContatos mapa = new AgendaContatos();
        mapa.adicionarContato("Bernardo", "555555");
        assertEquals("555555", mapa.buscarTelefone("Bernardo")); 
    }
    
    @Test
    public void buscarTelefoneEmListaMaior() {
        AgendaContatos mapa = new AgendaContatos();
        mapa.adicionarContato("Bernardo5", "555555");
        mapa.adicionarContato("Bernardo1", "111111");
        mapa.adicionarContato("Bernardo2", "222222");
        mapa.adicionarContato("Bernardo8", "888888");
        mapa.adicionarContato("Bernardo3", "333333");
        mapa.adicionarContato("Bernardo4", "444444");
        mapa.adicionarContato("Bernardo0", "000000");
        assertEquals("333333", mapa.buscarTelefone("Bernardo3")); 
    }
    
    @Test
    public void buscarTelefoneInvalido() {
        AgendaContatos mapa = new AgendaContatos();
        mapa.adicionarContato("Bernardo", "555555");
        assertNull(mapa.buscarTelefone("Bernardodsmd")); 
    }
    
    @Test
    public void buscarNomeEmAgendaVazia() {
        AgendaContatos mapa = new AgendaContatos();
        assertNull(mapa.buscarNome("555555"));        
    }
    
    @Test
    public void buscarNomeValido() {
        AgendaContatos mapa = new AgendaContatos();
        mapa.adicionarContato("Bernardo", "555555");
        assertEquals("Bernardo", mapa.buscarNome("555555"));        
    }
    
    @Test
    public void buscarNomeEmListaMaior() {
        AgendaContatos mapa = new AgendaContatos();
        mapa.adicionarContato("Bernardo5", "555555");
        mapa.adicionarContato("Bernardo1", "111111");
        mapa.adicionarContato("Bernardo2", "222222");
        mapa.adicionarContato("Bernardo8", "888888");
        mapa.adicionarContato("Bernardo3", "333333");
        mapa.adicionarContato("Bernardo4", "444444");
        mapa.adicionarContato("Bernardo0", "000000");
        assertEquals("Bernardo4", mapa.buscarNome("444444")); 
    }
    
    @Test
    public void buscarNomeInvalido() {
        AgendaContatos mapa = new AgendaContatos();
        mapa.adicionarContato("Bernardo", "555555");
        assertNull(mapa.buscarNome("555554"));        
    }    
}
