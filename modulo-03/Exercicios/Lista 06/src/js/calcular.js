function calcularMediaDeEpisodios( numEpisodios, divisor ) {
  return numEpisodios / divisor;
}

function somarEpisodios( series ) {
  let somaEpi = 0;
  series.forEach( e => somaEpi += e.numeroEpisodios );
  return somaEpi;
}

function calcularSalariosDeFuncionarios( qtdFuncionarios, salario ) {
  return qtdFuncionarios * salario;
}

function calcularTotalSalarios( serie ) {
  const salDiretores = calcularSalariosDeFuncionarios( serie.diretor.length, 100000 );
  const salElenco = calcularSalariosDeFuncionarios( serie.elenco.length, 40000 );
  return salDiretores + salElenco;
}
