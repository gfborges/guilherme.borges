function anoDeEstreiaInvalido( anoEstreia ) {
  const anoAtual = new Date();
  return anoAtual.getFullYear() < anoEstreia;
}

function estreouApos( anoEstreia, anoLimite ) {
  return anoEstreia >= anoLimite;
}

function propriedadeInvalida( propriedade ) {
  return !propriedade;
}

function possuiCampoInvalido( serie ) {
  for ( campo in serie )
    if ( propriedadeInvalida( serie[campo] ) )
      return true;
}
