Array.prototype.invalidas = function() {
  const comAnoInvalido = this.filter( e => anoDeEstreiaInvalido( parseInt( e.anoEstreia, 10 ) ) );
  const comCampoInvalido = this.filter( e => possuiCampoInvalido( e ) );
  let seriesInvalidas = comAnoInvalido.concat( comCampoInvalido );
  return `Séries Inválidas: ${ seriesInvalidas.map( e => e.titulo ).join(' - ') }`;
}

Array.prototype.filtrarPorAno = function( ano ) {
  return this.filter( e => estreouApos( parseInt( e.anoEstreia, 10 ), ano ) );
}

Array.prototype.procurarPorNome = function( nome ) {
  return this.some( e => e.elenco.some( e => e.includes( nome ) ) );
}

Array.prototype.mediaDeEpisodios = function() {
  return calcularMediaDeEpisodios( somarEpisodios( this ), this.length );
}

Array.prototype.totalSalarios = function( indice ) {
  const total = calcularTotalSalarios( this[indice] );
  return total.toLocaleString( 'pt-br', { style: 'currency', currency: 'BRL' } );
}

Array.prototype.queroGenero = function( genero ) {
  const contemGenero = this.filter( e => e.genero.includes( genero ) );
  return contemGenero.map( e => e.titulo );
}

Array.prototype.queroTitulo = function( titulo ) {
  const porTitulo = this.filter( e => e.titulo.includes( titulo ) );
  return porTitulo.map( e => e.titulo );
}
