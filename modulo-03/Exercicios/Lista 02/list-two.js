function imprimir(num, milhar, decimal) {
  var up = Math.ceil(num*100)/100;
  var str = parseFloat(up).toFixed(2).split('.');
  var end = decimal + str[1];
  var count = 0
  var limit = 0;

  if (num < 0) {
    limit = 1;
  }

  for (let i = str[0].length-1; i >= limit; i--) {
    if (count < 3) {
      end = str[0][i] + end;
    }
    else {
      end = str[0][i] + milhar + end;
      count = 0;
    }
    count++
  }

  return end;
}

function imprimirBRL(num) {
  if (num < 0) {
    return "-R$ " + imprimir(num, ".", ",");
  }
  return  "R$ " + imprimir(num, ".", ",");
}

function imprimirGBP(num) {
  if (num < 0) {
    return "-£ " + imprimir(num, ",", ".");
  }
  return  "£ " + imprimir(num, ",", ".");
}

function imprimirFR(num) {
  if (num < 0) {
    return "-" + imprimir(num, ".", ",") + " €";
  }
  return  imprimir(num, ".", ",") + " €";
}
