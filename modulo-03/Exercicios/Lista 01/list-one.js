function calcularCirculo(obj) {
  if (obj.tipoCalculo === "A") {
    return obj.raio * obj.raio * Math.PI;
  }
  if (obj.tipoCalculo === "C") {
    return 2 * obj.raio * Math.PI;
  }
}

function naoBissexto(ano) {
    if (ano%4 !== 0) {
      return true;
    }
    if (ano%100 !== 0) {
      return false;
    }
    if (ano%400 === 0) {
      return false;
    }
    else return true;
}

function somarPares(array) {
  var soma = 0;
  for (var i = 0; i < array.length; i+=2) {
    soma += array[i];
  }
    return soma;
}

function adicionar(a) {
  return function (b) { return a + b }
}

function imprimirBRL(num) {
  var up = Math.ceil(num*100.0)/100.0;
  var str = parseFloat(up).toFixed(2).split('.');
  var end = "," + str[1];
  var count = 0;
  var start = "R$ ";
  var limit = 0;

  if (num < 0) {
    start = "-" + start;
    limit = 1;
  }

  for (let i = str[0].length-1; i >= limit; i--) {
    if (count < 3) {
      end = str[0][i] + end;
    }
    else {
      end = str[0][i] + "." + end;
      count = 0;
    }
    count++
  }

  return start + end;
}

function imprimirBRL2(num) {
  var up = Math.ceil(num*100)/100;
  return up.toLocaleString('pt-BR', {style: 'currency', currency:'BRL'})
}

function imprimirBRL3(num) {
  var up = Math.ceil(num*100)/100;
  var numero = up.toFixed(2).split('.');
  numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
  var neg = "-";
  var res = numero.join(',');
  if (num < 0) {
    res = neg + res;
  }
  return res;
}
