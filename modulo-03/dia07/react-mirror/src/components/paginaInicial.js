import React, { Component } from 'react';

import ListaEpisodios from '../models/listaEpisodios';
import EpisodiosApi from '../models/episodiosApi';

import EpisodioUi from './episodioUi';

import MensagemFlash from './shared/mensagemFlash';
import MeuInputNumero from './shared/meuInputNumero';
import MeuBotao from './shared/meuBotao';

import './paginaInicial.css';


export default class PaginaInicial extends Component {
  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    // definindo estado inicial de um componente
    this.state = {

      // episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirMensagem: false,
      deveExibirErro: false,
      deveExibirCampo: false,
      mensagemAlerta: '',
      mensagemCampo: ''
    }
  }

  componentDidMount() {    
    // this.episodiosApi.resetarDadosByDefault()
    this.episodiosApi.buscar()
      .then( episodiosDoServidor => {
        this.listaEpisodios = new ListaEpisodios( episodiosDoServidor.data )
        this.setState( {episodio: this.listaEpisodios.episodioAleatorio})
      } )
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( { episodio, deveExibirMensagem: false, deveExibirCampo: false,
      deveExibirErro: false, mensagemCampo: '' } )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    if ( !episodio.avaliado ) {
      this.setState( { episodio, deveExibirCampo: true, mensagemCampo: 'Qual sua nota para este episódio?' } )
    } else {
      this.setState( { episodio } )
    }
  }

  exibeMensagem = ( mensagemAlerta, corFundo ) => {
    this.setState( { mensagemAlerta, corFundo, deveExibirMensagem: true } )
  }

  registrarNota = evt => {
    const { episodio } = this.state
    const nota = evt.target.value
    if ( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota ).then( () => {
        this.exibeMensagem( 'Nota registrada com sucesso!', 'verde' )
        this.setState( { deveExibirCampo: false, mensagemCampo: '' } )
      })      
    } else {
      this.exibeMensagem( 'Informar uma nota válida (entre 1 e 5)', 'vermelho' )
    }   
  }

  atualizarMensagem = (devoExibir) => {
    this.setState(  { deveExibirMensagem: devoExibir })
  }

  atualizarErro = (devoExibir) => {
    this.setState(  {deveExibirErro: devoExibir })
  }

  render() {
    const { episodio, deveExibirMensagem, mensagemAlerta, corFundo, deveExibirErro, deveExibirCampo, mensagemCampo } = this.state
    const { listaEpisodios } = this
    return (
      !episodio ? (
        <h3>Aguarde...</h3>
      ) : (
        <React.Fragment>
          <MensagemFlash
            atualizarMensagem={ this.atualizarMensagem } segundos={ 5 }
            deveExibirMensagem={ deveExibirMensagem } mensagemAlerta={ mensagemAlerta } corFundo={ corFundo }/>
          <EpisodioUi episodio={ episodio } />
          <MeuInputNumero placeholder="1 a 5" visivel={ deveExibirCampo } obrigatorio={ true }
            atualizarErro={ this.atualizarErro } deveExibirErro={ deveExibirErro }
            registrarNota={ this.registrarNota } mensagemCampo={ mensagemCampo }/>
          <div className="botoes">
            <MeuBotao texto="Próximo" cor="verde" funcao={ this.sortear }/>
            <MeuBotao texto="Já assisti" cor="azul" funcao={ this.marcarComoAssistido }/>
            <MeuBotao texto="Ver notas!" cor="vermelho" temLink={ true } link={ "/avaliacoes" } dadosNavegacao={ { listaEpisodios } }/>
          </div>
        </React.Fragment>
      )      
    );
  }

}
