import React, { Component } from 'react'
import EpisodiosApi from '../models/episodiosApi';
import ListaNotas from '../models/listaNotas';
// import ListaEpisodios from '../models/listaEpisodios';



export default class ListaAvaliacoes extends Component {
    constructor( props ) {
      super( props )
      this.episodiosApi = new EpisodiosApi()    
    }

    componentDidMount() {
      this.episodiosApi.buscarNotas().then( notasDoServidor => {
        this.listaNotas = new ListaNotas( notasDoServidor.data )
      })
    }

    ordenarPorExibicao( lista ) {
      return lista.avaliados.sort( (a, b) => { 
        a.temporadaEpisodio.localeCompare( b.temporadaEpisodio )
      })
    }

    renderizarElementos( lista ) {
      const ordenados = this.ordenarPorExibicao( lista )
      return ordenados.map( e => <li key={ e.id }>{e.nome} avaliado em {e.nota}</li>)
    }
    
    render() {
      const { listaEpisodios } = this.props.location.state
      return (
        <React.Fragment>
          <h2>Lista de avaliações</h2>
          <ul>
            { this.renderizarElementos( listaEpisodios ) }
          </ul>
        </React.Fragment>
      )
    }
}
