import React, { Component } from "react";
import "./mensagemFlash.css";
import PropTypes from 'prop-types';

export default class MensagemFlash extends Component {

  constructor( props ) {
    super( props )
    this.idsTimeouts = []
    this.animacao = ''
  }

  fechar = () => {
    this.props.atualizarMensagem( false )
  }

  componentWillUnmount() {
    this.idsTimeouts.forEach( clearTimeout )
  }

  componentDidUpdate( prevProps ) {
    const { deveExibirMensagem, segundos } = this.props
    if ( prevProps.deveExibirMensagem !== deveExibirMensagem ) {
      this.idsTimeouts.forEach( clearTimeout )
      this.idsTimeouts.push( setTimeout( () => { this.fechar() }, segundos * 1000 ) )
    }
  }

  render() {
    const { deveExibirMensagem, mensagemAlerta, corFundo } = this.props;

    
    if ( deveExibirMensagem ) {
      this.animacao = 'fade-in'
    } else {
      this.animacao = 'fade-out'
    }
    return <span onClick={ this.fechar } className={ `flash ${ corFundo } ${ this.animacao }` }>{ mensagemAlerta }</span>
  }
}

MensagemFlash.propTypes = {
  mensagemAlerta: PropTypes.string.isRequired,
  deveExibirMensagem: PropTypes.bool.isRequired,
  atualizarMensagem: PropTypes.func.isRequired,
  corFundo: PropTypes.oneOf( [ 'verde', 'vermelho' ] ),
  segundos: PropTypes.number
}

MensagemFlash.defaultProps = {
  corFundo: 'verde',
  segundos: 3
}
