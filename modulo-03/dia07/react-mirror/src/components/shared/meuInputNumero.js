import React, { Component } from "react";
import PropTypes from 'prop-types';
import './meuInputNumero.css';

export default class MeuInputNumero extends Component {

  perderFoco = evt => {
    if ( this.props.obrigatorio && !evt.target.value ) {
      this.props.atualizarErro( true )
    } else {
      this.props.atualizarErro( false )
      this.props.registrarNota( evt )
    }
  }

  render() {
    const { placeholder, visivel, mensagemCampo, deveExibirErro } = this.props

    return (
    <React.Fragment>
      <div> 
        { mensagemCampo && <span  >{ mensagemCampo }</span> } 
        { visivel && <input type="textfield" className={`meuInput ${ deveExibirErro ? 'campo-obrigatorio' : '' }`} placeholder={ placeholder } onBlur={ this.perderFoco } /> }
        { deveExibirErro && <span className="mensagem-erro">*obrigatório*</span> }
      </div>           
    </React.Fragment>
    )
  }
}

MeuInputNumero.propTypes = {
  visivel: PropTypes.bool.isRequired,
  atualizarErro: PropTypes.func.isRequired,
  deveExibirErro: PropTypes.bool.isRequired,
  registrarNota: PropTypes.func,
  obrigatorio: PropTypes.bool,
  mensagemCampo: PropTypes.string,
  placeholder: PropTypes.string  
}

MeuInputNumero.defaultProps = {
  obrigatorio: false
}
