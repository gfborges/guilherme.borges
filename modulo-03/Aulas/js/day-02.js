var hello = "global"
function start() {
  console.log(hello)
  var hello = "oi";
}

function escopo() {

//let serve como variavel de bloco
  for (let i = 0; i < 5; i++) {
    console.log(i);
  }
  console.log(i);
}

//IIFE função unica que chame a si mesmo no momento que é declara
(function()
 {
  'use strict';

}());

(function externa() {
  function interna() {
    var x = "x";
    return x;
  }
  //
  console.log(`o valor de x é: ${ interna() }`)
})()
